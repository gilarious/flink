package examples.windows.triggers;


import examples.data.SampleData;
import examples.data.SampleDataWatermarkAssigner;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.datastream.WindowedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

import static jdk.nashorn.internal.objects.NativeArray.reduce;

/**
 * Created on April 27th, 2018
 */
@Data
@Slf4j
public class EventTimeTriggerExample {

    private static long WINDOW_SECONDS = 10;

    public static void main(String[] args) throws Exception {

        // get the execution environment
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        // set time characteristics
        env.setStreamTimeCharacteristic( TimeCharacteristic.EventTime );

        // create input data
        Collection<SampleData> sampleDataCollection = new LinkedList<>(Arrays.asList(
                new SampleData(1524847128000L, "test")));

        // create input stream
        DataStream<SampleData> sampleDataStream = env.fromCollection( sampleDataCollection );
//        SingleOutputStreamOperator<Tuple2<String, SampleData>> sampleDataTupleStream = sampleDataDataStream.map((MapFunction<SampleData, Tuple2<String, SampleData>>) value -> new Tuple2<>(value.getName(), value));

        //
        WindowedStream<SampleData, Tuple, TimeWindow> sampleDataTupleTimeWindowWindowedStream = sampleDataStream
                .assignTimestampsAndWatermarks(new SampleDataWatermarkAssigner())
                .keyBy("name")
                .timeWindow(Time.seconds(WINDOW_SECONDS));

        SingleOutputStreamOperator<SampleData> reducedSampleDataSingleOutputStreamOperator = sampleDataTupleTimeWindowWindowedStream.reduce((ReduceFunction<SampleData>) (value1, value2) -> {
            value1.incCount();
            return value1;
        });

        // print the results with a single thread, rather than in parallel
        reducedSampleDataSingleOutputStreamOperator.print().setParallelism(1);

        env.execute("Event Time Trigger Example" );
    }


}

