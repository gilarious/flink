package examples.data;


import lombok.Data;

/**
 * Created on April 27th, 2018
 */
@Data
public class SampleData {

    final private long eventTime;
    final private String name;
    private int count;

    public SampleData(long eventTime, String name) {
        this.eventTime = eventTime;
        this.name = name;
        this.count = 1;
    }

    public long getEventTime() {
        return eventTime;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int addition) {
        this.count += addition;
    }

    public void incCount() {
        this.count += 1;
    }

}
