package examples.data;


import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks;
import org.apache.flink.streaming.api.watermark.Watermark;

import javax.annotation.Nullable;

/**
 * Created on April 27th, 2018
 */
public class SampleDataWatermarkAssigner implements AssignerWithPunctuatedWatermarks<SampleData> {


    @Nullable
    @Override
    public Watermark checkAndGetNextWatermark(SampleData lastElement, long extractedTimestamp) {
        return lastElement == null ? new Watermark(extractedTimestamp) : null;
    }

    @Override
    public long extractTimestamp(SampleData element, long previousElementTimestamp) {
        return element.getEventTime();
    }
}
